<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class mBlocks extends CI_Model {

	public $block;

	public function load(string $identity) {
		$query = $this->db->query(
			"SELECT * 
			 FROM cms_block 
			 WHERE code = '$identity'"
		);
		$this->block = $query->row();
		return $this;
	}

	public function toHtml() {
		if ($this->block->is_active) {
			$this->load->view(
				'blocks/' . $this->block->template . '.php',
				array(
					'code'    => $this->block->code,
					'content' => $this->block->content
				)
			);
		}
	}

}