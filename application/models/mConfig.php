<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mConfig extends CI_Model {

	protected $_config;

	public function load(string $code) {
		$query = $this->db->select('value')
		                  ->from('core_config')
		                  ->where('code', $code)
		                  ->get();
        $this->_config = $query->row();
        return $this;
	}

	public function get() {
		return $this->_config->value;
	}

}